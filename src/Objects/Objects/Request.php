<?php

namespace MicroHop\Objects;

use MicroHop\Classes\Input\Input;
use MicroHop\Objects\Enums\RequestMethodEnum;
use MicroHop\Objects\Enums\RequestTypeEnum;
use MicroHop\Objects\Interfaces\NonExecutableInterface;

class RequestObject implements NonExecutableInterface
{
	/**
	 * @var string $uri
	 */
	private string $uri = '';

	/**
	 * @var RequestTypeEnum $type
	 */
	private RequestTypeEnum $type;

	/**
	 * @var RequestMethodEnum
	 */
	private RequestMethodEnum $method;

	/**
	 * @var Input $input
	 */
	private Input $input;

	public function __construct(
		RequestTypeEnum $type
	)
	{
		$this->type = $type;

		// uri
		$this->uri = filter_input(INPUT_GET, '_url', FILTER_SANITIZE_URL)  ?? '/';

		// method
		$this->method = RequestMethodEnum::get();
		if (!empty($_SERVER['REQUEST_METHOD']))
		{
			if (in_array($_SERVER['REQUEST_METHOD'], RequestMethodEnum::getNames()))
			{
				$this->method = RequestMethodEnum::make($_SERVER['REQUEST_METHOD']);
			}
		}

		// input
		$this->input = new Input();
	}

	/**
	 * @return string
	 */
	public function getUri () : string
	{
		return $this->uri;
	}

	/**
	 * @return RequestTypeEnum
	 */
	public function getType () : RequestTypeEnum
	{
		return $this->type;
	}

	/**
	 * @return RequestMethodEnum
	 */
	public function getMethod () : RequestMethodEnum
	{
		return $this->method;
	}

	/**
	 * @return Input
	 */
	public function getInput () : Input
	{
		return $this->input;
	}
}