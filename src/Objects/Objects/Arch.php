<?php

namespace MicroHop\Objects;

use MicroHop\Classes\Assign\Assign;
use MicroHop\Objects\Interfaces\NonExecutableInterface;
use MicroHop\Classes\Env\EnvClass;

class ArchObject implements NonExecutableInterface
{
	/**
	 * @var RequestObject $Request
	 */
	private RequestObject $Request;

	/**
	 * @var EnvClass $Env
	 */
	private EnvClass $Env;

	/**
	 * @var Assign $Assign
	 */
	private Assign $Assign;

	/**
	 * @var ResponseObject
	 */
	private ResponseObject $Response;

	/**
	 * ArchObject constructor.
	 * @param RequestObject $Request
	 * @param EnvClass $Env
	 * @throws Exceptions\AssignException
	 */
	public function __construct(
		RequestObject $Request,
		EnvClass $Env
	)
	{
		$this->Request = $Request;
		$this->Env = $Env;
		$this->Assign = new Assign($this->Request->getType());
		$this->Response = new ResponseObject($this->Request->getType());
	}

	/**
	 * @return RequestObject
	 */
	public function getRequest () : RequestObject
	{
		return $this->Request;
	}

	/**
	 * @return EnvClass
	 */
	public function getEnv () : EnvClass
	{
		return $this->Env;
	}

	/**
	 * @return Assign
	 */
	public function getAssign () : Assign
	{
		return $this->Assign;
	}

	/**
	 * @return ResponseObject
	 */
	public function getResponse () : ResponseObject
	{
		return $this->Response;
	}
}