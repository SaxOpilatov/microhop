<?php

namespace MicroHop\Objects;

use MicroHop\Classes\Assign\Assign;
use MicroHop\Classes\Response\Api;
use MicroHop\Classes\Response\Cli;
use MicroHop\Objects\Enums\RequestTypeEnum;
use MicroHop\Objects\Exceptions\ResponseException;
use MicroHop\Objects\Interfaces\NonExecutableInterface;
use MicroHop\Objects\Interfaces\ResponseInterface;

class ResponseObject implements NonExecutableInterface
{
	/**
	 * @var RequestTypeEnum
	 */
	private RequestTypeEnum $type;

	/**
	 * ResponseObject constructor.
	 * @param RequestTypeEnum $type
	 */
	public function __construct(RequestTypeEnum $type)
	{
		$this->type = $type;
	}

	/**
	 * @return ResponseInterface
	 * @throws ResponseException
	 */
	private function getClass () : ResponseInterface
	{
		switch (mb_strtolower($this->type->getValue()))
		{
			case 'api':
				$class = new Api();
				break;

			case 'cli':
				$class = new Cli();
				break;

			default:
				throw new ResponseException('[[ error.response.type ]]');
		}

		return $class;
	}

	/**
	 * @param Assign $assign
	 * @throws ResponseException
	 */
	public function printData (Assign $assign) : void
	{
		$class = $this->getClass();

		// set headers
		$headers = $class->headers();
		if (!empty($headers))
		{
			foreach ($headers as $key => $value)
			{
				header("{$key}: {$value}");
			}
		}

		// set body
		if (mb_strtolower($this->type->getValue()) !== 'cli')
		{
			$class->output($assign);
		}

		// set output
		$output = ob_get_contents();
		ob_end_clean();

		print($output);
	}
}