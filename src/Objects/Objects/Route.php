<?php

namespace MicroHop\Objects;

use MicroHop\Objects\Enums\RequestMethodEnum;
use MicroHop\Objects\Enums\RequestTypeEnum;
use MicroHop\Objects\Interfaces\NonExecutableInterface;

class RouteObject implements NonExecutableInterface
{
	/**
	 * @var string $uri
	 */
	private string $uri;

	/**
	 * @var RequestMethodEnum $type
	 */
	private RequestMethodEnum $type;

	/**
	 * @var string $controller
	 */
	private string $controller;

	/**
	 * @var string $action
	 */
	private string $action;

	/**
	 * RouteObject constructor.
	 * @param string $uri
	 * @param RequestMethodEnum $type
	 * @param string $controller
	 * @param string $action
	 */
	public function __construct(string $uri, RequestMethodEnum $type, string $controller, string $action)
	{
		$this->uri = $uri;
		$this->type = $type;
		$this->controller = $controller;
		$this->action = $action;
	}

	/**
	 * @return string
	 */
	public function getUri () : string
	{
		return $this->uri;
	}

	/**
	 * @return RequestMethodEnum
	 */
	public function getType () : RequestMethodEnum
	{
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getController () : string
	{
		return $this->controller;
	}

	/**
	 * @return string
	 */
	public function getAction () : string
	{
		return $this->action;
	}
}