<?php

namespace MicroHop\Objects\Abstractions;

use MicroHop\Objects\ArchObject;
use MicroHop\Objects\Exceptions\AssignException;
use MicroHop\Objects\Exceptions\ControllerException;
use MicroHop\Objects\Exceptions\DefaultException;
use MicroHop\Objects\Interfaces\ControllerInterface;

abstract class ControllerAbstraction implements ControllerInterface
{
	/**
	 * @var ArchObject $arch
	 */
	protected ArchObject $arch;

	/**
	 * ControllerAbstraction constructor.
	 * @param ArchObject $arch
	 */
	public function __construct(ArchObject $arch)
	{
		$this->arch = $arch;
	}

	/**
	 * On init method
	 *      -> executes before controller main method
	 *      -> can include any checks
	 *      -> non-returnable
	 */
	public function onInit(): void
	{
		// implement this method in your controller if needed
	}

	/**
	 * Need auth method
	 *      -> executes before controller main method
	 *      -> Returns boolean value
	 */
	public function needAuth(): bool
	{
		// implement this method in your controller if needed
		return false;
	}

	/**
	 * Use redis cache or not
	 *      -> don't use if null
	 *      -> if string then use this sub
	 */
	public function useRedis(): ?string
	{
		// implement this method in your controller if needed
		return null;
	}

	/**
	 * @throws ControllerException
	 * @throws DefaultException
	 *
	 * Index method
	 *      -> should be implemented in your controller
	 */
	public function index(): void
	{
		// implement this method in your controller
		throw new ControllerException('[[ error.controller.index ]]');
	}
}