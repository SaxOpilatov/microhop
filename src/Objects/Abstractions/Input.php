<?php

namespace MicroHop\Objects\Abstractions;

use MicroHop\Objects\Interfaces\InputInterface;

abstract class InputAbstraction implements InputInterface
{
	abstract public function getAll(): array;
	abstract public function getValue(string $name, int $filter);

	/**
	 * @param string $name
	 * @return string|null
	 */
	public function strVal (string $name) : ?string
	{
		return $this->getValue($name, FILTER_SANITIZE_STRIPPED);
	}

	/**
	 * @param string $name
	 * @return int|null
	 */
	public function intVal (string $name) : ?int
	{
		return $this->getValue($name, FILTER_SANITIZE_NUMBER_INT);
	}

	/**
	 * @param string $name
	 * @return float|null
	 */
	public function floatVal (string $name) : ?float
	{
		return $this->getValue($name, FILTER_SANITIZE_NUMBER_FLOAT);
	}
}