<?php

namespace MicroHop\Objects\Abstractions;

use MicroHop\Objects\Exceptions\DefaultException;
use MicroHop\Objects\Interfaces\AssignInterface;

abstract class AssignAbstraction implements AssignInterface
{
	/**
	 * @var array $outputData
	 */
	protected array $outputData = [];

	/**
	 * @var array $outputErrors
	 */
	protected array $outputErrors = [];

	abstract public function data(string $key, $value): void;
	abstract public function error(string $code, string $description): void;

	/**
	 * @param int|null $code
	 * @throws DefaultException
	 */
	public function stop (?int $code = 200) : void
	{
		if (!in_array($code, [200, 204, 400, 403, 404, 500, 501])) $code = 200;
		throw new DefaultException('[[ error.stop ]]', $code);
	}

	/**
	 * @return array|null
	 */
	public function getOutput() : ?array
	{
		return [
			'data' => $this->outputData,
			'errors' => $this->outputErrors
		];
	}
}