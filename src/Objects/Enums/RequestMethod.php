<?php

namespace MicroHop\Objects\Enums;

use Spatie\Enum\Enum;

/**
 * Class RequestMethodEnum
 * @package MicroHop\Objects\Enums
 * @method static self any()
 * @method static self get()
 * @method static self post()
 * @method static self put()
 * @method static self delete()
 */
class RequestMethodEnum extends Enum
{

}