<?php

namespace MicroHop\Objects\Enums;

use Spatie\Enum\Enum;

/**
 * Class RequestTypeEnum
 * @package MicroHop\Objects\Enums
 * @method static self api()
 * @method static self cli()
 */
class RequestTypeEnum extends Enum
{

}