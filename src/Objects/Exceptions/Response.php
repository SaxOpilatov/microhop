<?php

namespace MicroHop\Objects\Exceptions;

use Throwable;

class ResponseException extends DefaultException
{
	/**
	 * ResponseException constructor.
	 * @param string $message
	 */
	public function __construct($message = "")
	{
		parent::__construct($message, 500);
	}
}