<?php

namespace MicroHop\Objects\Exceptions;

class ControllerException extends DefaultException
{
	/**
	 * ControllerException constructor.
	 * @param string $message
	 */
	public function __construct($message = "")
	{
		parent::__construct($message, 500);
	}
}