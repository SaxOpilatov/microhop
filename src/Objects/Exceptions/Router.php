<?php

namespace MicroHop\Objects\Exceptions;

use Throwable;

class RouterException extends DefaultException
{
	/**
	 * RouterException constructor.
	 * @param string $message
	 */
	public function __construct($message = "")
	{
		parent::__construct($message, 500);
	}
}