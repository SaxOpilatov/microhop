<?php

namespace MicroHop\Objects\Exceptions;

use Throwable;

class AssignException extends DefaultException
{
	/**
	 * AssignException constructor.
	 * @param string $message
	 */
	public function __construct($message = "")
	{
		parent::__construct($message, 500);
	}
}