<?php

namespace MicroHop\Objects\Exceptions;

use Throwable;

class DirException extends DefaultException
{
	/**
	 * DirException constructor.
	 * @param string $message
	 */
	public function __construct($message = "")
	{
		parent::__construct($message, 500);
	}
}