<?php

namespace MicroHop\Objects\Interfaces;

/**
 * Interface ExecutableInterface
 * @package MicroHop\Objects\Interfaces
 */
interface ExecutableInterface extends NonExecutableInterface
{

}