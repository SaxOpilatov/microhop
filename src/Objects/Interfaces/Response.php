<?php

namespace MicroHop\Objects\Interfaces;

use MicroHop\Classes\Assign\Assign;

/**
 * Interface AssignInterface
 * @package MicroHop\Objects\Interfaces
 */
interface ResponseInterface extends ExecutableInterface
{
	public function headers(): array;
	public function output(Assign $assign): void;
}