<?php

namespace MicroHop\Objects\Interfaces;

/**
 * Interface AssignInterface
 * @package MicroHop\Objects\Interfaces
 */
interface AssignInterface extends ExecutableInterface
{
	public function data(string $key, $value): void;
	public function error(string $code, string $description): void;
	public function stop(?int $code = 200): void;
	public function getOutput(): ?array;
}