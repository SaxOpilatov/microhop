<?php

namespace MicroHop\Objects\Interfaces;

/**
 * Interface InputInterface
 * @package MicroHop\Objects\Interfaces
 */
interface InputInterface extends ExecutableInterface
{
	public function getAll(): array;
	public function getValue(string $name, int $filter);
	public function strVal(string $name): ?string;
	public function intVal(string $name): ?int;
	public function floatVal(string $name): ?float;
}