<?php

namespace MicroHop\Objects\Interfaces;

/**
 * Interface ExecutableInterface
 * @package MicroHop\Objects\Interfaces
 */
interface ControllerInterface extends ExecutableInterface
{
	public function needAuth(): bool;
	public function useRedis(): ?string;
	public function onInit(): void;
	public function index(): void;
}