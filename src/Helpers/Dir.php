<?php

namespace MicroHop\Helpers;

use Composer\Autoload\ClassLoader;
use MicroHop\Objects\Exceptions\DirException;
use MicroHop\Objects\Interfaces\ExecutableInterface;

class DirHelper implements ExecutableInterface
{
	/**
	 * @param string $dir
	 * @return bool
	 */
	public static function isExist (?string $dir = '/') : bool
	{
		return file_exists($dir);
	}

	/**
	 * @param string $dir
	 * @return string
	 */
	public static function fixPath (string $dir) : string
	{
		if (mb_substr($dir, -1) !== '/') $dir .= '/';
		return $dir;
	}

	/**
	 * @param string $dir
	 * @return string
	 * @throws DirException
	 */
	public static function getPath (string $dir) : string
	{
		if (mb_substr($dir, 0, 1) === '/') $dir = mb_substr($dir, 1);
		$workDir = null;

		$paths = [
			'app' => self::getPathRoot(),
			'hop' => __DIR__ . '/../'
		];

		if (file_exists($paths['app'] . $dir))
		{
			$workDir = $paths['app'] . $dir;
		} else {
			if (file_exists($paths['hop'] . $dir))
			{
				$workDir = $paths['hop'] . $dir;
			} else {
				throw new DirException('[[ error.dir.define ]]');
			}
		}

		$workDir = self::fixPath($workDir);
		return $workDir;
	}

	/**
	 * @return string
	 * @throws DirException
	 */
	public static function getPathRoot () : string
	{
		try {
			$reflection = new \ReflectionClass(ClassLoader::class);
			return dirname($reflection->getFileName()) . '/../../';
		} catch (\Exception $e) {
			throw new DirException($e->getMessage());
		}
	}

	/**
	 * @param string $dir
	 * @param string|null $ext
	 * @return array
	 */
	public static function getFiles (string $dir, ?string $ext = null) : array
	{
		$data = [];
		$files = scandir($dir);

		if (count($files) > 2)
		{
			for ($i=2; $i<count($files); $i++)
			{
				if ($ext !== null)
				{
					if (pathinfo($files[$i], PATHINFO_EXTENSION) === $ext)
					{
						$data[] = self::fixPath($dir) . $files[$i];
					}
				} else {
					$data[] = self::fixPath($dir) . $files[$i];
				}
			}
		}

		return $data;
	}
}