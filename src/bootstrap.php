<?php

namespace MicroHop;

use MicroHop\Objects\{ArchObject, Enums\RequestTypeEnum, RequestObject};
use MicroHop\Classes\Controller\ControllerTrait;
use MicroHop\Classes\Database\DatabaseTrait;
use MicroHop\Classes\Env\EnvTrait;
use MicroHop\Classes\Router\Router;

class Bootstrap
{
	use EnvTrait;
	use ControllerTrait;
	use DatabaseTrait;

	/**
	 * @var ArchObject $Arch
	 */
	private ArchObject $Arch;

	/**
	 * @var RequestTypeEnum $RequestType
	 */
	private RequestTypeEnum $RequestType;

	/**
	 * Bootstrap constructor.
	 */
	public function __construct()
	{
		ob_start();
		$this->RequestType = RequestTypeEnum::api();
	}

	/**
	 * Start app
	 * ---------
	 * @throws Objects\Exceptions\AssignException
	 * @throws Objects\Exceptions\DefaultException
	 * @throws Objects\Exceptions\DirException
	 * @throws Objects\Exceptions\RouterException
	 */
	public function run () : void
	{
		// set env
		$this->setEnv();

		// set arch
		$this->setArch();

		// load db
		$this->loadDatabase($this->Arch);

		// init router
		$route = Router::init($this->Arch);

		// set controller
		try {
			$this->setController($this->Arch, $route);
		} catch (\Exception $e) {
			// nothing
		}

		// output
		$this->output();
	}

	/**
	 * @return Bootstrap
	 */
	public function setCli () : Bootstrap
	{
		$this->RequestType = RequestTypeEnum::cli();
		return $this;
	}

	/**
	 * @throws Objects\Exceptions\AssignException
	 * @throws Objects\Exceptions\DefaultException
	 */
	private function setArch () : void
	{
		$this->Arch = new ArchObject(
			new RequestObject(
				$this->RequestType
			),
			$this->getEnvClass()
		);
	}

	/**
	 * @throws Objects\Exceptions\ResponseException
	 */
	private function output () : void
	{
		$this->Arch->getResponse()->printData($this->Arch->getAssign());
	}
}