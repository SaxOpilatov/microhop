<?php

namespace MicroHop\Classes\Controller;

use MicroHop\Objects\ArchObject;
use MicroHop\Objects\RouteObject;
use MicroHop\Objects\Exceptions\ControllerException;
use MicroHop\Objects\Exceptions\DirException;

trait ControllerTrait
{
	/**
	 * @param ArchObject $arch
	 * @param RouteObject $route
	 * @throws ControllerException
	 * @throws DirException
	 * @throws \MicroHop\Objects\Exceptions\DefaultException
	 * @throws \MicroHop\Objects\Exceptions\ResponseException
	 */
	public function setController (ArchObject $arch, RouteObject $route)
	{
		$controller = new ControllerClass($arch, $route);
		$controller->run();
	}
}