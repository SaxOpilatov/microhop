<?php

namespace MicroHop\Classes\Controller;

use MicroHop\Helpers\DirHelper;
use MicroHop\Objects\Abstractions\ControllerAbstraction;
use MicroHop\Objects\ArchObject;
use MicroHop\Objects\Exceptions\AssignException;
use MicroHop\Objects\Exceptions\ControllerException;
use MicroHop\Objects\Exceptions\ResponseException;
use MicroHop\Objects\RouteObject;
use MicroHop\Objects\Exceptions\DirException;
use MicroHop\Objects\Exceptions\DefaultException;

class ControllerClass
{
	/**
	 * @var ArchObject $arch
	 */
	private ArchObject $arch;

	/**
	 * @var RouteObject
	 */
	private RouteObject $route;

	/**
	 * ControllerClass constructor.
	 * @param ArchObject $arch
	 * @param RouteObject $route
	 */
	public function __construct(ArchObject $arch, RouteObject $route)
	{
		$this->arch = $arch;
		$this->route = $route;
	}

	/**
	 * @throws ControllerException
	 * @throws DirException
	 * @throws ResponseException
	 * @throws DefaultException
	 */
	public function run () : void
	{
		// check class
		$ucController = ucfirst($this->route->getController());
		$ucAction = ucfirst($this->route->getAction());

		$class = "MicroHop\\App\\Controllers\\{$ucController}Controller\\{$ucAction}Action";
		if (!class_exists($class))
		{
			throw new ControllerException('[[ error.controller.class ]]');
		}

		/**
		 * @var ControllerAbstraction $Controller
		 */
		$Controller = new $class($this->arch);
		if (!is_subclass_of($Controller, ControllerAbstraction::class))
		{
			throw new ControllerException('[[ error.controller.subclass ]]');
		}

		// need auth?
		if ($Controller->needAuth())
		{
			// ToDo: auth check
		}

		// onInit exec
		$Controller->onInit();

		// use redis?
		if ($Controller->useRedis() !== null && !empty($this->arch->getEnv()->getVar('REDIS_ADDR')))
		{
			// ToDo: load from redis
		} else {
			$this->runIndex($Controller);
		}
	}

	/**
	 * @param ControllerAbstraction $Controller
	 * @throws ControllerException
	 * @throws DefaultException
	 * @throws ResponseException
	 */
	private function runIndex (ControllerAbstraction $Controller) : void
	{
		try {
			$Controller->index();
		} catch (AssignException $e) {
			throw new ResponseException($e->getMessage());
		}
	}
}