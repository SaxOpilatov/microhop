<?php

namespace MicroHop\Classes\Database;

use MicroHop\Classes\Env\EnvClass;
use MicroHop\Helpers\DirHelper;
use MicroHop\Objects\ArchObject;
use MicroHop\Objects\Exceptions\DirException;
use Propel\Runtime\Connection\ConnectionManagerSingle;
use Propel\Runtime\Propel;

class DatabaseClass
{
	/**
	 * @param ArchObject $arch
	 * @throws DirException
	 */
	public static function loadDatabase (ArchObject $arch)
	{
//		self::loadModels($arch);

		$serviceContainer = Propel::getServiceContainer();
		$serviceContainer->checkVersion('2.0.0-dev');
		$serviceContainer->setAdapterClass('default', 'pgsql');

		$manager = new ConnectionManagerSingle();
		$manager->setConfiguration([
			'dsn' => 'pgsql:host=db;port=5432;dbname=db',
			'user' => 'dev',
			'password' => 'password',
			'settings' => [
				'charset' => 'utf8',
				'queries' => []
			],
			'classname' => '\\Propel\\Runtime\\Connection\\ConnectionWrapper',
			'model_paths' => [
				0 => 'src',
				1 => 'vendor',
			]
		]);
		$manager->setName('default');
		$serviceContainer->setConnectionManager('default', $manager);
		$serviceContainer->setDefaultDatasource('default');
	}

	/**
	 * @param ArchObject $arch
	 * @throws DirException
	 */
	private static function loadModels (ArchObject $arch) : void
	{
		$rii = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(DirHelper::getPath($arch->getEnv()->getVar('DIR_MODELS'))));
		$files = [];

		foreach ($rii as $file)
		{
			if ($file->isDir()) continue;

			if (mb_strpos($file->getPathname(), '/Map/'))
			{
				$queue = 0;
			} else if (mb_strpos($file->getPathname(), '/Base/')) {
				$queue = 1;
			} else {
				$queue = 2;
			}

			$files[$queue][] = $file->getPathname();
		}

		if (!empty($files))
		{
			for ($i=0; $i<count($files); $i++)
			{
				foreach ($files[$i] as $file)
				{
					require_once $file;
				}
			}
		}
	}

	/**
	 * @return array
	 * @throws DirException
	 */
	public static function getConf () : array
	{
		$env = new EnvClass();
		$dbHost = $env->getVar('DB_HOST');
		$dbPort = $env->getVar('DB_PORT');
		$dbName = $env->getVar('DB_NAME');
		$dbUser = $env->getVar('DB_USER');
		$dbPass = $env->getVar('DB_PASS');

		return [
			'propel' => [
				'paths' => [
					'schemaDir' => DirHelper::getPathRoot(),
					'phpDir' => DirHelper::getPathRoot() . $env->getVar('DIR_MODELS'),
				],
				'database' => [
					'connections' => [
						'default' => [
							'adapter' => 'pgsql',
							'dsn' => "pgsql:host={$dbHost};port={$dbPort};dbname={$dbName}",
							'user' => $dbUser,
							'password' => $dbPass,
							'settings' => [
								'charset' => 'utf8'
							]
						]
					]
				]
			]
		];
	}
}