<?php

namespace MicroHop\Classes\Database;

use MicroHop\Objects\ArchObject;
use MicroHop\Objects\Exceptions\DirException;

trait DatabaseTrait
{
	/**
	 * @param ArchObject $arch
	 * @throws DirException
	 */
	public function loadDatabase (ArchObject $arch) : void
	{
		DatabaseClass::loadDatabase($arch);
	}
}