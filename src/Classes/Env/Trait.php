<?php

namespace MicroHop\Classes\Env;

trait EnvTrait
{
	/**
	 * @var string|null $envName
	 */
	private ?string $envName = null;

	/**
	 * @var string|null $envPath
	 */
	private ?string $envPath = null;

	/**
	 * @param string|null $path
	 * @return object
	 */
	public function setEnvPath (?string $path = null) : object
	{
		$this->envPath = $path;
		return $this;
	}

	/**
	 * @param string|null $name
	 * @return object
	 */
	public function setEnvName (?string $name = null) : object
	{
		$this->envName = $name;
		return $this;
	}

	/**
	 *
	 */
	public function setEnv () : void
	{
		EnvClass::load($this->envPath, $this->envName);
	}

	/**
	 * @return EnvClass
	 */
	public function getEnvClass () : EnvClass
	{
		return new EnvClass();
	}
}