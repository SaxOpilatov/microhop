<?php

namespace MicroHop\Classes\Env;

use Dotenv\Dotenv;

class EnvClass
{
	/**
	 * @param string|null $path
	 * @param string|null $name
	 */
	public static function load (?string $path, ?string $name) : void
	{
		if (!empty($path))
		{
			$envFile = null;
			if (!empty($name))
			{
				$envFile = '.env.' . $name;
			}

			$envFilePath = $path . '/.env';
			if ($envFile)
			{
				$envFilePath = $path . '/' . $envFile;
			}

			if (file_exists($envFilePath))
			{
				$env = Dotenv::createImmutable($path, $envFile);
				$env->load();
			}
		}
	}

	/**
	 * @return array
	 */
	public function getAsArray () : array
	{
		$data = [
			'DIR_ROUTES' => 'routes',
			'DIR_CONTROLLERS' => 'app/controllers',
			'DIR_MODELS' => 'app/models',

			'DB_HOST' => 'db',
			'DB_PORT' => 5320,
			'DB_NAME' => 'db',
			'DB_USER' => 'dev',
			'DB_PASS' => 'password'
		];

		if (!empty($_ENV))
		{
			foreach ($_ENV as $key => $value)
			{
				$data[$key] = $value;
			}
		}

		return $data;
	}

	/**
	 * @param string $name
	 * @return mixed|null
	 */
	public function getVar (string $name)
	{
		$data = $this->getAsArray();

		if (empty($data[$name]))
		{
			return null;
		}

		return $data[$name];
	}
}