<?php

namespace MicroHop\Classes\Response;

use MicroHop\Classes\Assign\Assign;
use MicroHop\Objects\Interfaces\ResponseInterface;

class Cli implements ResponseInterface
{
	/**
	 * @return array
	 */
	public function headers(): array
	{
		return [];
	}

	public function output(Assign $assign): void
	{
		print_r($assign->getOutput());
	}
}