<?php

namespace MicroHop\Classes\Response;

use MicroHop\Classes\Assign\Assign;
use MicroHop\Objects\Interfaces\ResponseInterface;

class Api implements ResponseInterface
{
	/**
	 * @return array
	 */
	public function headers(): array
	{
		return [
			'Content-Type' => 'application/json'
		];
	}

	/**
	 * @param Assign $assign
	 */
	public function output(Assign $assign): void
	{
		echo json_encode($assign->getOutput());
	}
}