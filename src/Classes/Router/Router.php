<?php

namespace MicroHop\Classes\Router;

use MicroHop\Helpers\DirHelper;
use MicroHop\Objects\ArchObject;
use MicroHop\Objects\Enums\RequestMethodEnum;
use MicroHop\Objects\Exceptions\RouterException;
use MicroHop\Objects\RouteObject;
use MicroHop\Objects\Exceptions\DirException;
use Phroute\Phroute\Dispatcher;
use Phroute\Phroute\RouteCollector;

class Router
{
	/**
	 * @var ArchObject $arch
	 */
	private static ArchObject $arch;

	/**
	 * @var RouteObject[] $routeList
	 */
	private static array $routeList = [];

	/**
	 * @var RouteCollector
	 */
	private static RouteCollector $Router;

	/**
	 * @param ArchObject $arch
	 * @return RouteObject
	 * @throws DirException
	 * @throws RouterException
	 */
	public static function init (ArchObject $arch) : RouteObject
	{
		// inits
		self::$arch = $arch;
		self::$routeList = self::getAllRoutes();
		self::$Router = new RouteCollector();

		// wrap routes
		self::wrapper();

		// dispatch
		try {
			$dispatcher = new Dispatcher(self::$Router->getData());
			return $dispatcher->dispatch(
				mb_strtoupper(self::$arch->getRequest()->getMethod()->getValue()),
				self::$arch->getRequest()->getUri()
			);
		} catch (\Throwable $e) {
			return new RouteObject(
				self::$arch->getRequest()->getUri(),
				self::$arch->getRequest()->getMethod(),
				'default',
				'Error404'
			);
		}
	}

	/**
	 * Wrap routes
	 */
	private static function wrapper ()
	{
		self::$Router->any('/', fn() => new RouteObject(
			'/',
			self::$arch->getRequest()->getMethod(),
			'default',
			'empty'
		));

		if (!empty(self::$routeList))
		{
			foreach (self::$routeList as $routeObject)
			{
				$method = mb_strtolower($routeObject->getType()->getValue());
				$uri = $routeObject->getUri();
				self::$Router->{$method}($uri, fn() => $routeObject);
			}
		}
	}

	/**
	 * @param string $file
	 * @return RouteObject[]
	 * @throws RouterException
	 */
	private static function getRouteFile (string $file) : array
	{
		$routeList = [];
		$json = file_get_contents($file);
		$routePrefix = pathinfo($file, PATHINFO_FILENAME);

		try {
			$routeArray = json_decode($json, true);
		} catch (\Exception $e) {
			$routeArray = [];
		}

		if (!empty($routeArray))
		{
			foreach ($routeArray as $item)
			{
				try {
					if (
						!empty($item['uri']) &&
						!empty($item['type']) &&
						!empty($item['controller']) &&
						!empty($item['action'])
					)
					{
						try {
							$type = RequestMethodEnum::make(mb_strtolower($item['type']));
						} catch (\Exception $e) {
							throw new RouterException('[[ error.router.route.type ]]');
						}

						if ($routePrefix !== 'initialize')
						{
							$uri = '/' . $routePrefix . '/' . $item['uri'];
						} else {
							if ($item['uri'] === '/')
							{
								$uri = '/';
							} else {
								$uri = '/' . $item['uri'];
							}
						}

						$routeList[] = new RouteObject(
							$uri,
							$type,
							$item['controller'],
							$item['action']
						);
					} else {
						throw new RouterException('[[ error.router.route ]]');
					}
				} catch (\Exception $e) {
					throw new RouterException($e->getMessage());
				}
			}
		}

		return $routeList;
	}

	/**
	 * @return RouteObject[]
	 * @throws RouterException
	 * @throws DirException
	 */
	private static function getAllRoutes () : array
	{
		$path = DirHelper::getPath(self::$arch->getEnv()->getVar('DIR_ROUTES'));
		if (!DirHelper::isExist($path))
		{
			throw new RouterException('[[ error.router.dir ]]');
		}

		// get route files
		$files = DirHelper::getFiles($path, 'json');
		$routeList = [];

		if (!empty($files))
		{
			foreach ($files as $file)
			{
				$routeList = [...self::getRouteFile($file)];
			}
		}

		return $routeList;
	}
}