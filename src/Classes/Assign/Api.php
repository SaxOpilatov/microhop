<?php

namespace MicroHop\Classes\Assign;

use MicroHop\Objects\Abstractions\AssignAbstraction;

class Api extends AssignAbstraction
{
	/**
	 * @param string $key
	 * @param $value
	 */
	public function data(string $key, $value): void
	{
		$this->outputData[$key] = $value;
	}

	/**
	 * @param string $code
	 * @param string $description
	 */
	public function error(string $code, string $description): void
	{
		$this->outputErrors[] = [
			'code' => $code,
			'description' => $description
		];
	}
}