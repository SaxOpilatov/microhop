<?php

namespace MicroHop\Classes\Assign;

use MicroHop\Classes\Console\Log;
use MicroHop\Objects\Abstractions\AssignAbstraction;

class Cli extends AssignAbstraction
{
	/**
	 * @param string $key
	 * @param $value
	 */
	public function data(string $key, $value): void
	{
		Log::log($value, 'green');
	}

	/**
	 * @param string $code
	 * @param string $description
	 */
	public function error(string $code, string $description): void
	{
		Log::log("{$code}: {$description}", 'red');
	}
}