<?php

namespace MicroHop\Classes\Assign;

use MicroHop\Objects\Abstractions\AssignAbstraction;
use MicroHop\Objects\Enums\RequestTypeEnum;
use MicroHop\Objects\Exceptions\AssignException;

class Assign extends AssignAbstraction
{
	/**
	 * @var AssignAbstraction $childClass;
	 */
	private AssignAbstraction $childClass;

	/**
	 * Assign constructor.
	 * @param RequestTypeEnum $requestType
	 * @throws AssignException
	 */
	public function __construct(RequestTypeEnum $requestType)
	{
		switch (mb_strtolower($requestType->getValue()))
		{
			case 'api':
				$this->childClass = new Api();
				break;

			case 'cli':
				$this->childClass = new Cli();
				break;

			default:
				throw new AssignException('[[ error.assign.type ]]');
		}
	}

	/**
	 * @param string $key
	 * @param $value
	 * @throws AssignException
	 */
	public function data(string $key, $value): void
	{
		try {
			$this->childClass->data($key, $value);
		} catch (AssignException $e) {
			throw new AssignException('[[ error.assign.child.data ]]');
		}
	}

	/**
	 * @param string $code
	 * @param string $description
	 * @throws AssignException
	 */
	public function error(string $code, string $description): void
	{
		try {
			$this->childClass->error($code, $description);
		} catch (AssignException $e) {
			throw new AssignException('[[ error.assign.child.error ]]');
		}
	}

	/**
	 * @return array
	 */
	public function getOutput() : array
	{
		return [
			'data' => $this->childClass->outputData,
			'errors' => $this->childClass->outputErrors
		];
	}
}