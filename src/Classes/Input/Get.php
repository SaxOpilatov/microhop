<?php

namespace MicroHop\Classes\Input;

use MicroHop\Objects\Abstractions\InputAbstraction;

class GetInput extends InputAbstraction
{
	/**
	 * Unsafe function
	 * @return array
	 */
	public function getAll () : array
	{
		$data = [];

		foreach ($_GET as $key => $value)
		{
			$data[$key] = filter_input(INPUT_GET, $key, FILTER_UNSAFE_RAW);
		}

		return $data;
	}

	/**
	 * Use only if really needed
	 * @param string $name
	 * @param int $filter
	 * @return mixed|null
	 */
	public function getValue (string $name, int $filter)
	{
		if (!in_array( $name, array_keys($this->getAll()) )) return null;
		return filter_input(INPUT_GET, $name, $filter);
	}
}