<?php

namespace MicroHop\Classes\Input;

use MicroHop\Objects\Abstractions\InputAbstraction;

class ArgInput extends InputAbstraction
{
	/**
	 * @var array $availableShortOptions
	 */
	private array $availableShortOptions = [
		"p:", // part (e.g.: fix, deploy)
		"a:", // action (e.g.: scriptName)
	];

	/**
	 * @var array $availableLongOptions
	 */
	private array $availableLongOptions = [];

	/**
	 * Unsafe function
	 * @return array
	 */
	public function getAll () : array
	{
		return getopt(join("", $this->availableShortOptions), $this->availableLongOptions);
	}

	/**
	 * Use only if really needed
	 * @param string $name
	 * @param int $filter
	 * @return mixed|null
	 */
	public function getValue (string $name, int $filter)
	{
		$data = $this->getAll();
		if (!in_array( $name, array_keys($data) )) return null;
		return filter_var($data[$name], $filter);
	}
}