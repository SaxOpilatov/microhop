<?php

namespace MicroHop\Classes\Input;

use MicroHop\Objects\Abstractions\InputAbstraction;

class PostInput extends InputAbstraction
{
	/**
	 * Unsafe function
	 * @return array
	 */
	public function getAll () : array
	{
		$data = [];

		$input = file_get_contents("php://input");
		$postParams = json_decode($input,true);

		foreach ($postParams as $key => $value)
		{
			$data[$key] = filter_var($value, FILTER_UNSAFE_RAW);
		}

		return $data;
	}

	/**
	 * Use only if really needed
	 * @param string $name
	 * @param int $filter
	 * @return mixed|null
	 */
	public function getValue (string $name, int $filter)
	{
		$data = $this->getAll();
		if (!in_array( $name, array_keys($data) )) return null;
		return filter_var($data[$name], $filter);
	}
}