<?php

namespace MicroHop\Classes\Input;

class Input
{
	/**
	 * @var GetInput $get
	 */
	private GetInput $get;

	/**
	 * @var PostInput
	 */
	private PostInput $post;

	/**
	 * @var HeadersInput
	 */
	private HeadersInput $headers;

	/**
	 * @var ArgInput $arg
	 */
	private ArgInput $arg;

	/**
	 * Input constructor.
	 */
	public function __construct()
	{
		$this->get = new GetInput();
		$this->post = new PostInput();
		$this->headers = new HeadersInput();
		$this->arg = new ArgInput();
	}

	/**
	 * @return GetInput
	 */
	public function getParams () : GetInput
	{
		return $this->get;
	}

	/**
	 * @return PostInput
	 */
	public function postParams () : PostInput
	{
		return $this->post;
	}

	/**
	 * @return HeadersInput
	 */
	public function headersParams () : HeadersInput
	{
		return $this->headers;
	}

	/**
	 * @return ArgInput
	 */
	public function argParams () : ArgInput
	{
		return $this->arg;
	}
}