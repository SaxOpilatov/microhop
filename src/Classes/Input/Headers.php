<?php

namespace MicroHop\Classes\Input;

use MicroHop\Objects\Abstractions\InputAbstraction;

class HeadersInput extends InputAbstraction
{
	/**
	 * Unsafe function
	 * @return array
	 */
	public function getAll () : array
	{
		$data = [];
		$headers = getallheaders();

		foreach ($headers as $key => $value)
		{
			$data[$key] = filter_var($headers[$key], FILTER_UNSAFE_RAW);
		}

		return $data;
	}

	/**
	 * Use only if really needed
	 * @param string $name
	 * @param int $filter
	 * @return mixed|null
	 */
	public function getValue (string $name, int $filter)
	{
		$data = $this->getAll();
		if (!in_array( $name, array_keys($data) )) return null;
		return filter_var($data[$name], $filter);
	}
}

/**
 * if nginx
 */
if (!function_exists('getallheaders'))
{
	/**
	 * @return array
	 */
	function getallheaders()
	{
		if (!is_array($_SERVER)) {
			return array();
		}

		$headers = array();
		foreach ($_SERVER as $name => $value) {
			if (substr($name, 0, 5) == 'HTTP_') {
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}
		return $headers;
	}
}