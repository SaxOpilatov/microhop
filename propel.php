<?php

require_once __DIR__ . '/vendor/autoload.php';

use \MicroHop\Classes\Database\DatabaseClass;

return DatabaseClass::getConf();